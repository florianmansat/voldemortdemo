import org.apache.commons.lang.RandomStringUtils;
import voldemort.client.*;
import voldemort.versioning.*;

import java.nio.charset.Charset;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        String bootstrapUrl = "tcp://localhost:6666";
        StoreClientFactory factory = new SocketStoreClientFactory(new ClientConfig().setBootstrapUrls(bootstrapUrl));

        String bootstrapUrl2 = "tcp://localhost:6670";
        StoreClientFactory factory2 = new SocketStoreClientFactory(new ClientConfig().setBootstrapUrls(bootstrapUrl));

        // create a client that executes operations on a single store
        StoreClient<String, String> client = factory.getStoreClient("test");

        // create a client that executes operations on a single store
        StoreClient<String, String> client2 = factory.getStoreClient("test");


        // value.setObject("some_value");
        // client.put("some_key", value);

        System.out.println("---------- [ First cluster : localhost:6666 ] ----------");

        String str = RandomStringUtils.randomAlphanumeric(17).toUpperCase();
        System.out.println("Putting '"+str+"' in 'test-key'");
        client.put("test-key", str);
        Versioned<String> value = client.get("test-key");
        System.out.println("'test-key' : '"+value.getValue()+"'");

        System.out.println("---------- [ Second cluster : localhost:6670 ] ----------");
        Versioned<String> value2 = client2.get("test-key");
        System.out.println("'test-key' : '"+value2.getValue()+"'");


        System.out.println("\n.....\n");


        System.out.println("---------- [ First cluster : localhost:6666 ] ----------");
        System.out.println("Deleting 'test-key'");
        client.delete("test-key");
        Versioned<String> value3 = client.get("test-key");
        if (value3 == null) {
            System.out.println("'test-key' : 'null'");
        }
        else {
            System.out.println("'test-key' : '"+value3.getValue()+"'");
        }


        System.out.println("---------- [ Second cluster : localhost:6670 ] ----------");
        Versioned<String> value4 = client2.get("test-key");
        if (value4 == null) {
            System.out.println("'test-key' : 'null'");
        }
        else {
            System.out.println("'test-key' : '"+value4.getValue()+"'");
        }
    }
}
